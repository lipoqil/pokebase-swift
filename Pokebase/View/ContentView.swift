//
//  ContentView.swift
//  Pokebase
//
//  Created by Mailo Světel on 09/10/2020.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var networkManager = NetworkManager()

    var body: some View {
        NavigationView {
            List(networkManager.pokemons.detailedResults!) { pokemon in
                NavigationLink(destination: PokemonDetail(pokemon: pokemon)) {
                    PokemonRow(pokemon: pokemon)
                }
            }
            .navigationBarTitle(Text("Pokémon"))
            
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
