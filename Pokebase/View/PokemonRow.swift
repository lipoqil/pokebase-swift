//
//  PokemonRow.swift
//  Pokebase
//
//  Created by Mailo Světel on 09/10/2020.
//

import SwiftUI

struct PokemonRow: View {
    var pokemon: PokemonDetailData
    
    var body: some View {
        HStack {
            Text(pokemon.name.capitalized)
        }
    }
}

struct PokemonRow_Previews: PreviewProvider {
    static var previews: some View {
        PokemonRow(pokemon: pokemonDetailedData)
    }
}
