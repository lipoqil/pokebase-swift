//
//  PokemonDetail.swift
//  Pokebase
//
//  Created by Mailo Světel on 09/10/2020.
//

import SwiftUI

struct PokemonDetail: View {
    var pokemon: PokemonDetailData
    
    var body: some View {
        VStack {
          HStack {
            Text("Id")
            Text("\(pokemon.id)")
          }
          HStack {
            Text("Name")
            Text(pokemon.name.capitalized)
          }
          HStack {
            Text("Height")
            Text("\(pokemon.height)")
          }
          HStack {
            Text("Order")
            Text("\(pokemon.order)")
          }
          
        }
        .navigationBarTitle(pokemon.name.capitalized)
        
    }
}

struct PokemonDetail_Previews: PreviewProvider {
    static var previews: some View {
        PokemonDetail(pokemon: pokemonDetailedData)
    }
}
