//
//  PokebaseApp.swift
//  Pokebase
//
//  Created by Mailo Světel on 09/10/2020.
//

import SwiftUI

@main
struct PokebaseApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
