//
//  NetworkManager.swift
//  Pokebase
//
//  Created by Mailo Světel on 09/10/2020.
//

import Combine
import SwiftUI

class NetworkManager: ObservableObject {
  var didChange = PassthroughSubject<NetworkManager, Never>()
  @Published var pokemons = Pokemons(results: [], detailedResults: []) {
    didSet {
      didChange.send(self)
    }
  }

  init() {
    guard let url = URL(string: "https://pokeapi.co/api/v2/pokemon?limit=10&offset=0") else {
      return
    }
    URLSession.shared.dataTask(with: url) { (data, _, _) in
      guard let data = data else { return }
        
      var pokemons = try! JSONDecoder().decode(Pokemons.self, from: data)
      var results: [PokemonDetailData] = []
      for pokemon in pokemons.results {
        URLSession.shared.dataTask(with: pokemon.url) { (detailData, _, _) in
          guard let detailData = detailData else { return }

          let pokemonDetailData = try! JSONDecoder().decode(PokemonDetailData.self, from: detailData)
          results.append(pokemonDetailData)
        }.resume()
      }
      DispatchQueue.main.async {
        pokemons.detailedResults = results
        self.pokemons = pokemons
      }
    }.resume()
  }
}
