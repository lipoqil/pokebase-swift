//
//  Pokemons.swift
//  Pokebase
//
//  Created by Mailo Světel on 09/10/2020.
//

import Foundation

struct Pokemons: Codable {
    var results: [Pokemon]
    var detailedResults: [PokemonDetailData]?
}
