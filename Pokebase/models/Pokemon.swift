//
//  Pokemon.swift
//  Pokebase
//
//  Created by Mailo Světel on 09/10/2020.
//

import Foundation

struct Pokemon: Codable, Identifiable {
  let id = UUID()
  var name: String
  var url: URL
}
