//
//  Pokemon.swift
//  Pokebase
//
//  Created by Mailo Světel on 09/10/2020.
//

import Foundation

struct PokemonDetailData: Codable, Identifiable {
  let id: Int
  var height: Int
  var name: String
  var order: Int
}
